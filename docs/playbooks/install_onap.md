# install_onap playbook

## Purpose

Install ONAP on top of a Kubernetes cluster

## Roles

- orange.oom.external_requirements
- orange.oom.prepare
- orange.oom.configure
- orange.oom.launch
- orange.oom.wait
- orange.oom.generate_artifacts

## Mandatory parameters

TBD
