# create_vms playbook

## Purpose

Install the ORONAP vms and harden OS

## Roles

- orange.os_infra_manager.external_requirements
- orange.os_infra_manager.user
- orange.os_infra_manager.project
- orange.os_infra_manager.network
- orange.os_infra_manager.flavor
- orange.os_infra_manager.security
- orange.os_infra_manager.server_groups
- orange.os_infra_manager.servers
- orange.os_infra_manager.inventory
- orange.os_infra_manager.configure_disks
- orange.os_infra_manager.set_keys
- orange.server_hardening.external_requirements
- orange.server_hardening.cis_security

## Mandatory parameters

TBD
