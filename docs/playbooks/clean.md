# clean playbook

## Purpose

Clean environment before installation

## Roles

- orange.infra.include_vars
- orange.os_infra_manager.external_requirements
- orange.os_infra_manager.admin_clean

## Mandatory parameters

TBD
